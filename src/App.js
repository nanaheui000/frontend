// react import
import "./App.css";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Provider } from "react-redux";

// redux import
import configStore from "redux/configStore";

// components import
import GameActionRecordPage from "pages/gameActionRecordPage/GameActionRecordPage";

function App() {
  return (
    <DndProvider backend={HTML5Backend}>
      <Provider store={configStore}>
        <BrowserRouter>
          <Routes>
            <Route path="/:gameInfo" element={<GameActionRecordPage />} />
          </Routes>
        </BrowserRouter>
      </Provider>
    </DndProvider>
  );
}

export default App;
