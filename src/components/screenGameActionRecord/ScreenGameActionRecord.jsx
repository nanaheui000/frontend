// react import
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Toaster } from "react-hot-toast";

// redux import
import { useDispatch } from "react-redux";
import { GameStatusThunk } from "redux/modules/gameActionRecordSlice/GameActionRecordPostSlice";
import { GameActionRecordRallyThunk } from "redux/modules/gameActionRecordSlice/GameActionRecordRallySlice";
import { GameActionPlayerListThunk } from "redux/modules/gameActionRecordSlice/GameActionPlayerListSlice";

// components import
import FlexDiv from "components/common/FlexDiv";
import ScreenGameActionTop from "./components/screenGameActionTop/ScreenGameActionTop";
import ScreenGameActionMiddle from "./components/screenGameActionMiddle/ScreenGameActionMiddle";

const ScreenGameActionRecord = () => {
  const dispatch = useDispatch();
  const { gameInfo } = useParams();

  useEffect(() => {
    dispatch(GameActionPlayerListThunk(gameInfo));
    dispatch(GameActionRecordRallyThunk());
    dispatch(GameStatusThunk(gameInfo));
  }, [dispatch]);
  return (
    <FlexDiv width={"2160px"}>
      <Toaster />
      <ScreenGameActionTop />
      <ScreenGameActionMiddle />
    </FlexDiv>
  );
};

export default ScreenGameActionRecord;
