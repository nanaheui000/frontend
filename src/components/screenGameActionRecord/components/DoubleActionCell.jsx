// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { changeText } from "redux/modules/gameActionRecordSlice/GameActionRecordPostSlice";

// components import
import FlexDiv from "components/common/FlexDiv";
import HoverFlexDiv from "components/common/HoverFlexDiv";
import { actions } from "static/actionScenario";

const DoubleActionCell = ({ firstAction, secondAction }) => {
  const dispatch = useDispatch();
  const {
    GameActionRecordPostSlice: { text },
  } = useSelector(state => state);
  return (
    <HoverFlexDiv
      background={"white"}
      border={"1px solid black"}
      radius={"10px"}
      width={"15%"}
      height={"80%"}
      margin={"10px"}>
      <FlexDiv
        width={"100%"}
        margin={"5px 0"}
        padding={"0 10px"}
        justify={"start"}
        direction={"row"}
        onClick={() => dispatch(changeText(text + firstAction))}>
        <FlexDiv
          border={"1px solid black"}
          radius={"5px"}
          width={"20px"}
          height={"20px"}>
          {firstAction}
        </FlexDiv>

        <FlexDiv margin={"0 20px"}>{actions[firstAction]}</FlexDiv>
      </FlexDiv>
      <FlexDiv
        width={"100%"}
        margin={"5px 0"}
        justify={"start"}
        direction={"row"}
        onClick={() => dispatch(changeText(text + secondAction))}>
        <FlexDiv
          border={"1px solid black"}
          radius={"5px"}
          width={"20px"}
          height={"20px"}>
          {secondAction}
        </FlexDiv>

        <FlexDiv
          margin={"0 20px"}
          size={actions[secondAction].length > 6 ? "13px" : "16px"}>
          {actions[secondAction]}
        </FlexDiv>
      </FlexDiv>
    </HoverFlexDiv>
  );
};

export default DoubleActionCell;
