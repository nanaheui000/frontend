// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";
import ScreenGameActionLeftArea from "./ScreenGameActionLeftArea";
import ScreenGameActionScore from "./ScreenGameActionScore";
import ScreenGameActionRightArea from "./ScreenGameActionRightArea";

const ScreenGameActionTop = () => {
  return (
    <FlexDiv
      direction={"row"}
      width={"2160px"}
      height={"140px"}
      background={"#acca69"}
      padding={"10px 0"}>
      <ScreenGameActionLeftArea />
      <ScreenGameActionScore />
      <ScreenGameActionRightArea />
    </FlexDiv>
  );
};

export default ScreenGameActionTop;
