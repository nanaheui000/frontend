// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import CellBlock from "../CellBlock";
import FlexDiv from "components/common/FlexDiv";

const ScreenGameActionScore = () => {
  const {
    GameActionRecordPostSlice: { gameStatus, data, gameData },
    GameActionPlayerListSlice: { teamType },
  } = useSelector(state => state);

  const currentGameData = data[data.length - 1] ? data[data.length - 1] : {};
  const SpreadCell = () => {
    let arr = [
      <CellBlock
        key={"first"}
        first={"팀 명"}
        second={gameData.homeTeam}
        third={gameData.awayTeam}
        background={"white"}
      />,
    ];
    for (let i = 1; i < 6; i++) {
      const first = i;
      const second = data[i] ? data[i].homeSetScore : 0;
      const third = data[i] ? data[i].awaySetScore : 0;
      // const background = setNum === currentGameData.setNum ? "yellow" : "white";
      arr.push(
        <CellBlock
          key={first + second}
          first={first}
          second={second}
          third={third}
          background={"white"}
        />
      );
    }

    arr.push(
      <CellBlock
        key={"last"}
        first={"Total"}
        second={currentGameData.homeScoreSum}
        third={currentGameData.awayScoreSum}
        background={"white"}
      />
    );
    return arr;
  };
  return (
    <FlexDiv
      direction={"row"}
      width={"40%"}
      height={"100%"}
      justify={"space-between"}
      background={"#6df6ea"}
      border={"1px solid black"}>
      <FlexDiv width={"20%"}>
        <FlexDiv
          background={teamType === gameData.homeTeam ? "yellow" : "none"}
          size={"30px"}
          margin={"10px 0"}>
          {gameData.homeTeam}
        </FlexDiv>
        <FlexDiv size={"30px"} margin={"10px 0"}>
          {currentGameData.homeScoreSum}
        </FlexDiv>
      </FlexDiv>
      <FlexDiv width={"50%"} height={"100%"}>
        <FlexDiv width={"100%"} justify={"space-around"} direction={"row"}>
          <FlexDiv margin={"5px 10px"} size={"30px"}>
            {currentGameData.homeScore}
          </FlexDiv>
          <FlexDiv size={"30px"}>
            {!!gameStatus ? gameStatus : "GAME STATUS"}
          </FlexDiv>
          <FlexDiv margin={"5px 10px"} size={"30px"}>
            {currentGameData.awayScore}
          </FlexDiv>
        </FlexDiv>
        <FlexDiv width={"100%"} height={"50%"} direction={"row"}>
          {SpreadCell()}
        </FlexDiv>
      </FlexDiv>
      <FlexDiv width={"20%"}>
        <FlexDiv
          size={"30px"}
          margin={"10px 0"}
          background={teamType === gameData.awayTeam ? "yellow" : "none"}>
          {gameData.awayTeam}
        </FlexDiv>
        <FlexDiv size={"30px"} margin={"10px 0"}>
          {currentGameData.awayScoreSum}
        </FlexDiv>
      </FlexDiv>
    </FlexDiv>
  );
};

export default ScreenGameActionScore;
