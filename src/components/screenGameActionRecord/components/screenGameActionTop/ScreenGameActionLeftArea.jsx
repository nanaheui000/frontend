// react import
import React from "react";

// redux import
import { useDispatch, useSelector } from "react-redux";
import { changeSetNum } from "redux/modules/gameActionRecordSlice/GameActionRecordPostSlice";
// components import
import FlexDiv from "components/common/FlexDiv";
import SetStartButton from "../buttons/SetStartButton";
import SetEndButton from "../buttons/SetEndButton";
import InsertPlayButton from "../buttons/InsertPlayButton";
import SelectBox from "components/common/SelectBox";
import LineUpSettingButton from "../buttons/LineUpSettingButton";
import Button from "components/common/Button";

const ScreenGameActionLeftArea = () => {
  const dispatch = useDispatch();
  const {
    GameActionRecordPostSlice: { setNum },
  } = useSelector(state => state);
  const onChangeHandler = e => {
    dispatch(changeSetNum(e.target.value));
  };

  return (
    <FlexDiv width={"25%"} padding={"10px"} direction={"row"}>
      <FlexDiv width={"40%"}>
        <FlexDiv justify={"start"} direction={"row"} width={"100%"}>
          <SelectBox
            width={"60%"}
            height={"25px"}
            selected={setNum}
            handleSelect={onChangeHandler}
            selectList={[1, 2, 3, 4, 5]}
          />
        </FlexDiv>
        <FlexDiv width={"100%"} direction={"row"} justify={"start"}>
          <SetStartButton />
          <SetEndButton />
        </FlexDiv>
      </FlexDiv>
      <FlexDiv width={"40%"}>
        <FlexDiv direction={"row"} width={"100%"} justify={"end"}>
          <Button margin={"10px"}>선발 오더</Button>{" "}
          <Button margin={"10px"}>경고</Button>
        </FlexDiv>
        <FlexDiv direction={"row"} justify={"end"} width={"100%"}>
          <LineUpSettingButton />
          <InsertPlayButton />
        </FlexDiv>
      </FlexDiv>
    </FlexDiv>
  );
};

export default ScreenGameActionLeftArea;
