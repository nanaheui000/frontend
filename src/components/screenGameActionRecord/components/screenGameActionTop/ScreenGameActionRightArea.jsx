// react import
import React from "react";

// redux import

// components import
import FlexDiv from "components/common/FlexDiv";
import Button from "components/common/Button";

const ScreenGameActionRightArea = () => {
  return (
    <FlexDiv width={"25%"} direction={"row"} padding={"10px"}>
      <FlexDiv width={"70%"}>
        <FlexDiv direction={"row"}>
          <Button margin={"10px"}>데이터 검토</Button>
          <Button margin={"10px"}>코트체인지</Button>
          <Button margin={"10px"}>비디오판독 내역</Button>
        </FlexDiv>
        <FlexDiv direction={"row"}>
          <Button margin={"10px"}>타임</Button>
          <Button margin={"10px"}>포지션 폴트</Button>
          <Button margin={"10px"}>히트맵 입력</Button>
        </FlexDiv>
      </FlexDiv>
      <FlexDiv height={"100%"} width={"20%"}>
        <Button>경기 종료</Button>
      </FlexDiv>
    </FlexDiv>
  );
};

export default ScreenGameActionRightArea;
