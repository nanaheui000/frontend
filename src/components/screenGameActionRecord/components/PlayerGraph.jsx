// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "components/common/FlexDiv";
import PlayerCell from "./PlayerCell";

const PlayerGraph = ({ type }) => {
  const {
    GameActionPlayerListSlice: { player },
  } = useSelector(state => state);
  return (
    <FlexDiv height={"50%"} width={"100%"} position={"relative"}>
      <FlexDiv
        top={"0"}
        left={"0"}
        color={"white"}
        background={"blue"}
        direction={"row"}
        width={"80px"}
        height={"40px"}
        position={"absolute"}>
        {type}
      </FlexDiv>
      <FlexDiv height={"80%"} wrap={"nowrap"}>
        <PlayerCell
          participantName={"이름"}
          participantBIB={"등번호"}
          participantPosition={"포지션"}
          background={"#50bcdf"}
        />
        <FlexDiv
          overflow={"scroll"}
          height={"100%"}
          justify={"start"}
          wrap={"nowrap"}>
          {player.map((item, index) =>
            item.teamId === type ? (
              <PlayerCell
                key={item + index}
                participantName={item.participantName}
                participantBIB={item.participantBIB}
                participantPosition={item.participantPosition}
              />
            ) : null
          )}
        </FlexDiv>
      </FlexDiv>
    </FlexDiv>
  );
};

export default PlayerGraph;
