// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";

const CellBlock = ({ first, second, third, background }) => {
  return (
    <FlexDiv width={"13%"}>
      <FlexDiv width={"100%"} background={"#d9d9d9"} border={"1px solid black"}>
        {first}
      </FlexDiv>
      <FlexDiv
        width={"100%"}
        background={background}
        border={"1px solid black"}>
        {second}
      </FlexDiv>
      <FlexDiv
        width={"100%"}
        background={background}
        border={"1px solid black"}>
        {third}
      </FlexDiv>
    </FlexDiv>
  );
};

export default CellBlock;
