// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";

const PlayerCell = ({
  participantName,
  participantBIB,
  participantPosition,
  background,
}) => {
  return (
    <FlexDiv direction={"row"}>
      <FlexDiv
        background={background}
        width={"80px"}
        height={"25px"}
        border={"1px solid black"}>
        {participantName}
      </FlexDiv>
      <FlexDiv
        background={background}
        border={"1px solid black"}
        width={"80px"}
        height={"25px"}>
        {participantBIB}
      </FlexDiv>
      <FlexDiv
        background={background}
        border={"1px solid black"}
        width={"80px"}
        height={"25px"}>
        {participantPosition}
      </FlexDiv>
    </FlexDiv>
  );
};
export default PlayerCell;
