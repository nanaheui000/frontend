// react import
import React from "react";

// components import
import styled from "styled-components";
import FlexDiv from "../../../../components/FlexDiv";
import HoverFlexDiv from "../../../../components/HoverFlexDiv";
import { MdOutlineCancel } from "react-icons/md";

const CommonModalHeader = ({ text, close }) => {
  return (
    <HeaderDiv>
      <FlexDiv size={"24px"}>{text}</FlexDiv>
      <FlexDiv>
        <HoverFlexDiv onClick={close}>
          <CancelIcon />
        </HoverFlexDiv>
      </FlexDiv>
    </HeaderDiv>
  );
};

export default CommonModalHeader;

const HeaderDiv = styled.div`
  width: 100%;
  height: 8%;
  border-bottom: 1px solid black;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  display: flex;
`;

const CancelIcon = styled(MdOutlineCancel)`
  width: 30px;
  height: 30px;
`;
