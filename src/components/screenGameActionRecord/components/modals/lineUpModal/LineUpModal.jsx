// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";

// components import
import styled from "styled-components";
import { BsFillArrowRightSquareFill } from "react-icons/bs";
import FlexDiv from "components/common/FlexDiv";
import CommonModalHeader from "./CommonModalHeader";
import ModalEntry from "./ModalEntry";
import LineUpPosition from "./lineUpPosition/LineUpPosition";
import Button from "components/common/Button";

const LineUpModal = () => {
  const dispatch = useDispatch();
  const {
    TimeModalSlice: { selectPlayerList },
  } = useSelector(state => state);
  const CloseModal = () => {
    dispatch(offModal());
  };

  return (
    <FlexDiv
      width={"50%"}
      height={"50%"}
      outline={"1px solid black"}
      justify={"space-between"}
      padding={"20px 40px"}
      background={"#d9d9d9"}
      z={3}
      margin={"50px"}>
      <CommonModalHeader text={"라인업 세팅"} close={CloseModal} />
      {/* center */}
      <FlexDiv
        direction={"row"}
        justify={"space-between"}
        height={"80%"}
        width={"100%"}>
        <ModalEntry />

        <LineUpPosition />
      </FlexDiv>
      {/* center */}
      <FlexDiv direction={"row"} width={"20%"} justify={"space-between"}>
        <Button width={"50px"} height={"30px"}>
          저장
        </Button>
        <Button width={"50px"} height={"30px"}>
          취소
        </Button>
      </FlexDiv>
    </FlexDiv>
  );
};

export default LineUpModal;

const SendArrow = styled(BsFillArrowRightSquareFill)`
  width: 30px;
  height: 30px;
`;
