// react import
import React from "react";

// redux import
import { useDispatch } from "react-redux";
import { onModal } from "redux/modules/gameActionRecordSlice/GameActionRecordLineUpSlice";

// components import
import Button from "components/common/Button";

const LineUpSettingButton = () => {
  const dispatch = useDispatch();
  return (
    <Button margin={"10px"} onClick={() => dispatch(onModal())}>
      라인업 세팅
    </Button>
  );
};

export default LineUpSettingButton;
