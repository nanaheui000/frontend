// react import
import React from "react";
import axios from "axios";
import { toast } from "react-hot-toast";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { postRallyReset } from "redux/modules/gameActionRecordSlice/GameActionRecordRallySlice";
import { GameActionRecordRallyThunk } from "redux/modules/gameActionRecordSlice/GameActionRecordRallySlice";

// components import
import Button from "components/common/Button";

const InsertPlayButton = () => {
  const dispatch = useDispatch();
  const {
    GameActionRecordRallySlice: { postRallyData },
  } = useSelector(state => state);
  const SendAxios = () => {
    axios.post("/api/play/insertPlay", postRallyData).then(() => {
      dispatch(GameActionRecordRallyThunk());
      toast.success("전송이 완료 되었습니다.");
    });
    dispatch(postRallyReset());
  };
  return (
    <Button margin={"10px"} onClick={SendAxios}>
      전송
    </Button>
  );
};

export default InsertPlayButton;
