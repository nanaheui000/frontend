// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { SetEndThunk } from "redux/modules/gameActionRecordSlice/GameActionRecordPostSlice";

// components import
import Button from "components/common/Button";

const SetEndButton = () => {
  const dispatch = useDispatch();
  const {
    GameActionRecordPostSlice: {
      gameData: { competitionCode, gender, gameCode },
      setNum,
    },
  } = useSelector(state => state);

  const setEndData = {
    competitionCode,
    gender,
    gameCode,
    rallySeq: 0,
    setNum,
    participantId: "END",
    mainAction: "END",
    teamId: "SYSTEM",
    homeScore: 0,
    awayScore: 0,
  };

  return (
    <Button margin={"0 5px"} onClick={() => dispatch(SetEndThunk(setEndData))}>
      세트 종료
    </Button>
  );
};

export default SetEndButton;
