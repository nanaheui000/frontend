// react import
import React from "react";

// redux import
import { useSelector, useDispatch } from "react-redux";
import { changeText } from "redux/modules/gameActionRecordSlice/GameActionRecordPostSlice";

// components import
import FlexDiv from "components/common/FlexDiv";
import HoverFlexDiv from "components/common/HoverFlexDiv";
import { actions } from "static/actionScenario";

const ActionCell = ({ action }) => {
  const dispatch = useDispatch();
  const {
    GameActionRecordPostSlice: { text },
  } = useSelector(state => state);
  return (
    <HoverFlexDiv
      background={"white"}
      border={"1px solid black"}
      radius={"10px"}
      width={"15%"}
      height={"80%"}
      margin={"10px"}
      onClick={() => dispatch(changeText(text + action))}>
      <FlexDiv justify={"space-evenly"} direction={"row"}>
        <FlexDiv
          border={"1px solid black"}
          radius={"5px"}
          width={"20px"}
          height={"20px"}>
          {action}
        </FlexDiv>

        <FlexDiv margin={"0 20px"}>{actions[action]}</FlexDiv>
      </FlexDiv>
    </HoverFlexDiv>
  );
};

export default ActionCell;
