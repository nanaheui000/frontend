// react import
import React, { useEffect } from "react";

// redux import
import { useDispatch, useSelector } from "react-redux";
import {
  changeText,
  checkActionArr,
} from "redux/modules/gameActionRecordSlice/GameActionRecordPostSlice";
import { changeTeam } from "redux/modules/gameActionRecordSlice/GameActionPlayerListSlice";
import { postRallyAdd } from "redux/modules/gameActionRecordSlice/GameActionRecordRallySlice";
// components import
import styled from "styled-components";
import { getKeyByValue } from "components/common/getKeyByValue";

import {
  actionCodeScenario,
  changeCase,
  pointArr,
} from "static/actionScenario";
import { toastObject } from "components/common/toast";

const RecordInput = ({ width, height }) => {
  const dispatch = useDispatch();
  const {
    GameActionPlayerListSlice: {
      HList,
      HBIB,
      WList,
      WBIB,
      homeTeam,
      awayTeam,
      teamType,
    },
    GameActionRecordRallySlice: { rallySeq },
    GameActionRecordPostSlice: {
      gameData: { competitionCode, gender, gameCode },
      setNum,
      text,
      actionArr,
    },
  } = useSelector(state => state);
  const teamId = teamType;
  const playerList = teamType === homeTeam ? HBIB : WBIB;

  const ChangeTeam = () => {
    teamType === homeTeam
      ? dispatch(changeTeam(awayTeam))
      : dispatch(changeTeam(homeTeam));
  };
  const CheckAction = () => {
    if (text[0] === "z" || text[0] === "c") {
      const teamResultCode = text[0];
      return {
        competitionCode,
        gender,
        gameCode,
        postCode: teamResultCode,
        player: teamId,
        gameAction: getKeyByValue(actionCodeScenario, teamResultCode),
        participantId: teamId,
        setNum,
        mainAction: teamResultCode,
        rallySeq,
        homeScore: teamResultCode === "z" && teamType === homeTeam ? 1 : 0,
        awayScore: teamResultCode === "z" && teamType === awayTeam ? 1 : 0,
        teamId,
      };
    } else {
      const split = text.split("");
      const postCode = text.substr(2);
      const playerNumber = split[0] + split[1];
      const player = getKeyByValue(playerList, playerNumber);
      const gameAction = getKeyByValue(actionCodeScenario, postCode);
      const participantId =
        teamType === homeTeam ? HList[player] : WList[player];

      let homeScore = 0;
      let awayScore = 0;

      if (pointArr.includes(gameAction) && teamType === homeTeam) {
        homeScore += 1;
      }
      if (pointArr.includes(gameAction) && teamType === awayTeam) {
        awayScore += 1;
      }

      return {
        competitionCode,
        gender,
        gameCode,
        postCode,
        player,
        gameAction,
        participantId,
        setNum,
        mainAction: postCode,
        rallySeq,
        homeScore,
        awayScore,
        teamId,
      };
    }
  };

  const EnterPost = () => {
    if (!actionArr.player) {
      toastObject.errorNotify("잘못된 입력입니다.");
    } else {
      dispatch(postRallyAdd(actionArr));
      dispatch(changeText(""));

      if (changeCase.includes(actionArr.gameAction)) {
        ChangeTeam();
      }
    }
  };
  window.onkeydown = e => {
    if (e.key === "ArrowUp" || e.key === "ArrowDown") {
      e.preventDefault();
    }
    if (e.key === "ArrowUp") {
      dispatch(changeTeam(homeTeam));
    } else if (e.key === "ArrowDown") {
      dispatch(changeTeam(awayTeam));
    } else if (e.key === "Enter") {
      EnterPost();
    }
  };
  useEffect(() => {
    dispatch(checkActionArr(CheckAction()));
  }, [text]);

  return (
    <Input
      width={width}
      height={height}
      value={text}
      onChange={e => dispatch(changeText(e.target.value))}
    />
  );
};

export default RecordInput;

const Input = styled.input`
  width: ${props => (props.width ? props.width : "200px")};
  height: ${props => (props.height ? props.height : "30px")};
  padding: 0 5px;
  margin: ${props => (props.margin ? props.margin : "10px")};
  font-size: 20px;
`;
