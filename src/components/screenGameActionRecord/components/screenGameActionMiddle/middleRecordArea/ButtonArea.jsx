// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";
import ActionCell from "../../ActionCell";
import DoubleActionCell from "../../DoubleActionCell";

const SpreadActionCell = text => {
  const arr = text.split("");
  return arr.map(item => <ActionCell key={item} action={item} />);
};

const ButtonArea = () => {
  return (
    <FlexDiv width={"100%"} height={"70%"} padding={"10px"}>
      <FlexDiv
        width={"100%"}
        height={"30%"}
        direction={"row"}
        justify={"start"}>
        <DoubleActionCell firstAction={"q"} secondAction={"Q"} />{" "}
        {SpreadActionCell("wert")}
      </FlexDiv>
      <FlexDiv
        width={"100%"}
        height={"30%"}
        direction={"row"}
        justify={"start"}>
        {SpreadActionCell("asdf")}
      </FlexDiv>
      <FlexDiv
        width={"100%"}
        height={"30%"}
        direction={"row"}
        justify={"start"}>
        {SpreadActionCell("z")}
        <DoubleActionCell firstAction={"x"} secondAction={"X"} />
        {SpreadActionCell("c")}
      </FlexDiv>
    </FlexDiv>
  );
};

export default ButtonArea;
