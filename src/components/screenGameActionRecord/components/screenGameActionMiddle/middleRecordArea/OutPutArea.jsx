// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";
import RecordInput from "./RecordInput";
import ActionOutPut from "./ActionOutPut";

const OutPutArea = () => {
  return (
    <FlexDiv width={"100%"} height={"25%"}>
      <FlexDiv
        width={"100%"}
        direction={"row"}
        justify={"start"}
        padding={"0 20px"}>
        <FlexDiv
          background={"blue"}
          border={"1px solid black"}
          width={"60px"}
          height={"30px"}
          color={"white"}>
          입력
        </FlexDiv>
        <RecordInput width={"60%"} height={"30px"} margin={"10px"} />
        <FlexDiv
          background={"blue"}
          border={"1px solid black"}
          width={"60px"}
          height={"30px"}
          color={"white"}>
          ENTER
        </FlexDiv>
      </FlexDiv>
      <FlexDiv width={"100%"} direction={"row"} justify={"start"}>
        <FlexDiv
          background={"blue"}
          border={"1px solid black"}
          width={"60px"}
          height={"30px"}
          color={"white"}>
          상세
        </FlexDiv>
        <ActionOutPut />
      </FlexDiv>
    </FlexDiv>
  );
};

export default OutPutArea;
