// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "components/common/FlexDiv";
import PlayerGraph from "../../PlayerGraph";

const PlayerInfoArea = () => {
  const {
    GameActionPlayerListSlice: { homeTeam, awayTeam },
  } = useSelector(state => state);
  return (
    <FlexDiv
      width={"20%"}
      height={"100%"}
      border={"1px solid black"}
      background={"white"}>
      <PlayerGraph type={homeTeam} />
      <PlayerGraph type={awayTeam} />
    </FlexDiv>
  );
};

export default PlayerInfoArea;
