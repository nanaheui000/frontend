// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";
import ButtonArea from "./ButtonArea";
import OutPutArea from "./OutPutArea";

const MiddleRecordArea = () => {
  return (
    <FlexDiv width={"50%"} height={"100%"} justify={"space-evenly"}>
      <ButtonArea />
      <OutPutArea />
    </FlexDiv>
  );
};

export default MiddleRecordArea;
