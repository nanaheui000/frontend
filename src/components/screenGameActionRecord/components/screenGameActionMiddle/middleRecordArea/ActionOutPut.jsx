// react import
import React from "react";

// redux import
import { useSelector } from "react-redux";

// components import
import FlexDiv from "components/common/FlexDiv";

const ActionOutPut = () => {
  const {
    GameActionRecordPostSlice: { actionArr },
  } = useSelector(state => state);
  return (
    <FlexDiv
      width={"60%"}
      height={"30px"}
      margin={"10px"}
      padding={"0 5px"}
      background={"white"}
      justify={"start"}
      direction={"row"}>
      <FlexDiv size={"20px"} margin={"0 20px 0 0"}>
        {actionArr.player}
      </FlexDiv>
      <FlexDiv size={"20px"} margin={"0 20px 0 0"}>
        {actionArr.gameAction}
      </FlexDiv>
    </FlexDiv>
  );
};

export default ActionOutPut;
