// react import
import React from "react";

// components import
import FlexDiv from "components/common/FlexDiv";
import MiddleRecordArea from "./middleRecordArea/MiddleRecordArea";
import PlayerInfoArea from "./middleRecordArea/PlayerInfoArea";

const ScreenGameActionMiddle = () => {
  return (
    <FlexDiv
      width={"2160px"}
      height={"500px"}
      direction={"row"}
      justify={"space-between"}
      background={"#acca69"}>
      <MiddleRecordArea />
      <PlayerInfoArea />
    </FlexDiv>
  );
};

export default ScreenGameActionMiddle;
