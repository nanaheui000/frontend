import React from "react";

const SelectBox = ({ selected, selectList, handleSelect, width, height }) => {
  return (
    <select
      onChange={handleSelect}
      value={selected}
      style={{ width: width, padding: "0 5px", margin: "10px", height }}>
      {selectList.map(item => (
        <option value={item} key={item}>
          {item + "세트"}
        </option>
      ))}
    </select>
  );
};

export default SelectBox;
