import styled from "styled-components";

const Button = styled.button`
  color: white;
  justify-content: center;
  align-items: center;
  display: ${props => (props.display ? props.display : "flex")};
  text-align: center;
  white-space: normal;
  border-radius: ${props => (props.radius ? props.radius : "none")};
  background-color: ${props => (props.background ? props.background : "blue")};
  width: ${props => (props.width ? props.width : "80px")};
  height: ${props => (props.height ? props.height : "40px")};
  margin: ${props => (props.margin ? props.margin : "none")};
  padding: ${props => (props.padding ? props.padding : "none")};
  font-size: ${props => (props.size ? props.size : "16px")};
  &:hover {
    cursor: pointer;
  }
`;

export default Button;
