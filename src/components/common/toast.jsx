// react import
import { toast } from "react-hot-toast";

export const toastObject = {
  notify: text => toast.success(text),
  errorNotify: text => toast.error(text),
};
