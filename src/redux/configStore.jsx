import { configureStore, combineReducers } from "@reduxjs/toolkit";
import GameActionPlayerListSlice from "./modules/gameActionRecordSlice/GameActionPlayerListSlice";
import GameActionRecordClickPointSlice from "./modules/gameActionRecordSlice/GameActionRecordClickPointSlice";
import GameActionRecordDeleteSlice from "./modules/gameActionRecordSlice/GameActionRecordDeleteSlice";
import GameActionRecordDragSlice from "./modules/gameActionRecordSlice/GameActionRecordDragSlice";
import GameActionRecordLineUpSlice from "./modules/gameActionRecordSlice/GameActionRecordLineUpSlice";
import GameActionRecordRallySlice from "./modules/gameActionRecordSlice/GameActionRecordRallySlice";
import GameActionRecordPostSlice from "./modules/gameActionRecordSlice/GameActionRecordPostSlice";

const rootReducer = combineReducers({
  GameActionPlayerListSlice,
  GameActionRecordClickPointSlice,
  GameActionRecordDeleteSlice,
  GameActionRecordDragSlice,
  GameActionRecordLineUpSlice,
  GameActionRecordRallySlice,
  GameActionRecordPostSlice,
});

const configStore = configureStore({ reducer: rootReducer });

export default configStore;
