import { createSlice, current } from "@reduxjs/toolkit";

const initialState = {
  isDragging: false,
  dragTarget: "",
  overTarget: "",
  positionObject: {
    gameCode: "",
    gender: "",
    rallySeq: 0,
    setNum: 0,
    homeFirst: "",
    homeSecond: "",
    homeThird: "",
    homeFourth: "",
    homeFifty: "",
    homeSixth: "",
    awayFirst: "",
    awaySecond: "",
    awayThird: "",
    awayFourth: "",
    awayFifty: "",
    awaySixth: "",
  },
};

const GameActionRecordDragSlice = createSlice({
  name: "GameActionRecordDragSlice",
  initialState,
  reducers: {
    onDragging: (state, action) => {
      state.isDragging = true;
      state.dragTarget = action.payload;
    },
    offDragging: (state, action) => {
      state.isDragging = false;
    },

    onOver: (state, action) => {
      state.overTarget = action.payload;
      state.positionObject[action.payload.rotation] =
        action.payload.participantId;
      console.log(current(state.positionObject));
    },
    deleteSelectPlayer: (state, action) => {
      state.positionObject[action.payload] = "";
    },
    setTarget: (state, action) => {
      state.dragTarget = action.payload;
    },
  },
});

export const {
  onDragging,
  offDragging,
  setTarget,
  onOver,
  onLeave,
  deleteSelectPlayer,
} = GameActionRecordDragSlice.actions;
export default GameActionRecordDragSlice.reducer;
