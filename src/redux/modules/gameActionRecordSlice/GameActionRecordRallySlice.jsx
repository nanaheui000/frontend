import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = { rallyData: [], rallySeq: 0, postRallyData: [] };

export const GameActionRecordRallyThunk = createAsyncThunk(
  "GameActionRecordRallyThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .get("/api/play/selectPlayList")
        .then(res => res.data.data);

      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const GameActionRecordRallySlice = createSlice({
  name: "GameActionRecordRallySlice",
  initialState,
  reducers: {
    postRallyReset: state => (state.postRallyData = []),
    postRallyAdd: (state, action) => {
      state.postRallyData = [...state.postRallyData, action.payload];
    },
    postRallyDelete: (state, action) => {
      state.postRallyData = state.postRallyData.filter(
        (item, index) => index !== action.payload - 1
      );
    },
  },
  extraReducers: {
    [GameActionRecordRallyThunk.fulfilled]: (state, action) => {
      state.rallyData = action.payload;
      state.rallySeq =
        action.payload.length > 0 ? action.payload[0].rallySeq + 1 : 1;
    },
  },
});

export const { postRallyReset, postRallyAdd, postRallyDelete } =
  GameActionRecordRallySlice.actions;
export default GameActionRecordRallySlice.reducer;
