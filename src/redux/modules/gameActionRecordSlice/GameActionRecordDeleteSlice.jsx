import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  delArr: [],
  delArrNum: 0,
  delStatus: false,
  deletePost: {},
};

export const GameActionRecordDeleteThunk = createAsyncThunk(
  "GameActionRecordDeleteThunk/get",
  async (payload, thunkAPI) => {
    try {
      const data = await axios
        .post("/api/play/deletePlay", payload)
        .then(res => console.log(res));

      return thunkAPI.fulfillWithValue(data);
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

const GameActionRecordDeleteSlice = createSlice({
  name: "SecondDataSlice",
  initialState,
  reducers: {
    onModal: (state, action) => {
      state.delArrNum = action.payload;
      state.delStatus = true;
    },
    offModal: state => {
      state.delStatus = false;
    },
    addDelPost: (state, action) => {
      state.deletePost = action.payload;
    },
    cancelDelPost: (state, action) => {
      state.deletePost = {};
    },
    setArr: (state, action) => {
      state.delArr = action.payload;
    },
  },
  extraReducers: {
    [GameActionRecordDeleteThunk.fulfilled]: (state, action) => {
      state = initialState;
    },
  },
});

export const { onModal, offModal, setArr, addDelPost, cancelDelPost } =
  GameActionRecordDeleteSlice.actions;
export default GameActionRecordDeleteSlice.reducer;
