// react import
import React from "react";

// components import
import ScreenGameActionRecord from "components/screenGameActionRecord/ScreenGameActionRecord";

const GameActionRecordPage = () => {
  return (
    <>
      <ScreenGameActionRecord />
    </>
  );
};

export default GameActionRecordPage;
